<nav class=" navbar navbar-default navbar-btn ">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
<!--        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Brand</a>
        </div>-->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <!--<ul class="nav navbar-nav ">-->
                <ul class = "nav nav-pills nav-justified">


                <li role="presentation" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" >Entreprise<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation"><a href="<?php echo site_url('entreprise/Entreprise_c/ajouterEntreprise'); ?>">Ajouter</a></li>
                        <li role="presentation"><a href="<?php echo site_url('entreprise/Entreprise_c/listerEntreprise'); ?>">Lister</a></li>
                    </ul>
                </li>

                <li role="presentation" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">Contact<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation"><a href="<?php echo site_url('contact/Contact_c/ajouter_Contact'); ?>">Ajouter</a></li>
                        <li role="presentation"><a href="<?php echo site_url('contact/Contact_c/afficher_Les_Contacts'); ?>">Lister</a></li>
                    </ul>
                </li>

                <li role="presentation" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">Etudiant<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation"><a href="<?php echo site_url('etudiant/Etudiant_c/ajouterEtudiant'); ?>">Ajouter</a></li>
                        <li role="presentation"><a href="<?php echo site_url('etudiant/Etudiant_c/listerEtudiant'); ?>">Lister</a></li>
                    </ul>
                </li>

                <li role="presentation" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">Intéraction<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation"><a href="<?php echo site_url('interaction/interaction_c/ajouter_une_interaction'); ?>">Ajouter</a></li>
                        <li role="presentation"><a href="<?php echo site_url('interaction/interaction_c/afficher_historique'); ?>">Lister</a></li>
                        <li role="presentation"><a href="<?php echo site_url('#'); ?>">Rechercher</a></li>
                    </ul>
                </li>

                <li role="presentation" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">Taxe d'apprentisssage<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation"><a href="<?php echo site_url('taxeApprentissage/taxeApprentissage_c/ajouterTaxeApprentissage'); ?>">Ajouter</a></li>
                        <li role="presentation"><a href="<?php echo site_url('taxeApprentissage/taxeApprentissage_c/listerTaxeApprentissage'); ?>">Lister</a></li>
                    </ul>
                </li>

                <li role="presentation" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">Utilisateurs<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation"><a href="<?php echo site_url('#'); ?>">Créer un compte</a></li>
                        <li role="presentation"><a href="<?php echo site_url('#'); ?>">Liste des utilisateurs</a></li>
                    </ul>
                </li>
            </ul>
            

        </div><!-- /.navbar-collapse -->
        <?php
            $attributes1 = array('class' => 'navbar-form navbar-left', 'role' => "search");
            echo form_open('accueil_c/rechercheEntreprise', $attributes1);
            ?>
            <div class="form-group">
                <label for="rechercheEntreprise" >Rechercher l'historique d'une entreprise</label>
                <input type="text" name="rechercheEntreprise" placeholder="Sopra" value="<?php echo set_value('rechercheEntreprise'); ?>">
            </div>
            <button type="submit" class="btn btn-default" name="valider">Valider</button>
            <?php echo form_close(); ?>

            <?php
            $attributes2 = array('class' => 'navbar-form navbar-left', 'role' => "search");
            echo form_open('accueil_c/rechercheEtudiant', $attributes2);
            ?>
            <div class="form-group">
                <label for="rechercheEtudiant" >Rechercher l'historique d'un étudiant</label>
                <input type="text" name="rechercheEtudiant" placeholder="Azizi Rania" value="<?php echo set_value('rechercheEtudiant'); ?>">
            </div>
            <button type="submit" class="btn btn-default" name="valider">Valider</button>
            <?php echo form_close(); ?>
    </div><!-- /.container-fluid -->
</nav>