<section>
    <form method="post" action="recherche">
        <label for="search" >Rechercher</label>
        <input type="text" name="search" placeholder="Sopra" value="<?php echo set_value('search'); ?>">
        <button type="submit" class="btn btn-default" name="valider">Valider</button>
    </form>
</section>

<section class="table-responsive">
    <h1>  <?php echo $message; ?></h1>
    <table class="table table-striped">
        <tr>
            <th>Date</th><th>Entreprise</th><th>Etudiant</th><th>Objet</th><th>Nombre</th><th></th>
        </tr>

        <?php foreach ($historique as $row): ?>
            <tr>
                <td><?php echo $row->dateInteraction ?></td>
                <td><?php echo $row->nom_entreprise ?></td>
                <td><?php echo $row->nom_etudiant ?> <?php echo $row->prenom_etudiant ?> </td>
                <td><?php echo $row->objet ?></td>
                <!--<td><?php echo $row->nb ?></td>-->
                <td align='right'><?php echo anchor('interaction/interaction_c/detail_historique/' . $row->idInteraction, 'Description', "class='btn btn-default'");
            ?></td>

            </tr>
        <?php endforeach ?>
    </table>

    <?php echo form_close(); ?>
</section>
