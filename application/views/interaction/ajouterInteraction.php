<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">

<script>
    $(function () {
        $("#datepicker").datepicker({
            altField: "#datepicker",
            closeText: 'Fermer',
            prevText: 'Précédent',
            nextText: 'Suivant',
            currentText: 'Aujourd\'hui',
            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
            dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
            weekHeader: 'Sem.',
            dateFormat: 'yy-mm-dd'
        });
    });
</script>
<section>
    <?php echo validation_errors(); ?>
    <h1> Nouvelle interaction </h1>

    <?php
    $attributes = array('class' => 'form-horizontal');
    echo form_open('interaction/interaction_c/ajouter_une_interaction', $attributes);
    ?>

    <?php
    $date = array('name' => 'dateinteraction',
        'placeholder' => 'Date du contact avec le contact de l entreprise',
        'id' => 'datepicker',
        'value' => set_value('dateinteraction'),
        'class' => 'form-control'
    );

    $auteur = array('name' => 'auteur',
        'placeholder' => 'Auteur',
        'id' => 'auteur',
        'value' => set_value('auteur'),
        'class' => 'form-control'
    );

    $objet = array('name' => 'objet',
        'placeholder' => 'objet',
        'id' => 'objet',
        'value' => set_value('objet'),
        'class' => 'form-control'
    );

    $description = array('name' => 'description',
        'placeholder' => 'Description',
        'id' => 'description',
        'value' => set_value('description'),
        'class' => 'form-control'
    );
    $etudiant = array('class' => 'form-control');
    $entreprise = array('class' => 'form-control');
    $valider = array('name' => 'valider',
        'value' => 'Valider',
        'class' => "btn btn-success");

    $entrepriseLabel = array('class' => "col-sm-2 control-label");
    $etudiantLabel = array('class' => "col-sm-2 control-label");
    $descriptionLabel = array('class' => "col-sm-2 control-label");
    $objetLabel = array('class' => "col-sm-2 control-label");
    $auteurLabel = array('class' => "col-sm-2 control-label");
    $dateLabel = array('class' => "col-sm-2 control-label");
    ?>

    <div class="form-group">
        <?php echo form_label('Date : ', 'datepicker', $dateLabel); ?>
        <div class="col-sm-10">
            <?php echo form_input($date); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo form_label('Auteur : ', 'auteur', $auteurLabel); ?>
        <div class="col-sm-10">
            <?php echo form_input($auteur); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="paint_source" class="col-sm-2 control-label">Source</label>
        <div class="col-sm-10">
            <select name = "source" id = "paint_source" class="form-control">
                <option>Telephone</option>
                <option>Fax</option>
                <option>Email</option>
                <option>Courier</option>
            </select>
        </div>
    </div>

    <div class="form-group">

        <?php echo form_label('Objet : ', 'objet', $objetLabel); ?>
        <div class="col-sm-10">
            <?php echo form_input($objet); ?>
        </div>
    </div>

    <div class="form-group">
        <?php
        echo form_label('Etudiant : ', ' ', $etudiantLabel);
        ?>
        <div class="col-sm-10">
            <?php echo form_dropdown('etudiant', $etudiants, '', $etudiant); ?>
        </div>
    </div>

    <div class="form-group">
        <?php
        echo form_label('Entreprise : ', ' ', $entrepriseLabel);
        ?>
        <div class="col-sm-10">
            <?php echo form_dropdown('entreprise', $entreprises, '', $entreprise); ?>
        </div>
    </div>

    <div class="form-group">

        <?php echo form_label('Description : ', 'description', $descriptionLabel); ?>
        <div class="col-sm-10">
            <?php echo form_textarea($description); ?>
        </div>
    </div>
    
    <a class="btn btn-primary" href = "<?php echo $pagePrecedente; ?>">Retour</a> 

    <?php echo form_close(); ?>
</section>