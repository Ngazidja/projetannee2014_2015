<section class="table-responsive">

    <!--Tableau qui liste les historiques-->

    <?php
    echo "<h1>" . $message . "</h1>";
    echo "<table class='table table-striped table-bordered table-hover'>"
    /*     * ***Date d'intéraction**** */
    . "<tr>"
    . "<th>Date de l'intération: </th>"
    . "<td>" . $historique->dateInteraction . "</td>"
    . "</tr>"
    /*     * ***Objet**** */
    . "<tr>"
    . "<th>Objet: </th>"
    . "<td>" . $historique->objet . "</td>"
    . "</tr>"

    /*     * ***Entreprise**** */
    . "<tr>"
    . "<th>Entreprise: </th>"
    . "<td>" . $historique->entreprise . "</td>"
    . "</tr>"

    /*     * ***Etudiant**** */
    . "<tr>"
    . "<th>Etudiant: </th>"
    . "<td>" . $historique->etudiant . "</td>"
    . "</tr>"

    /*     * ***Source**** */
    . "<tr>"
    . "<th>Source: </th>"
    . "<td>" . $historique->source . "</td>"
    . "</tr>"
    /*     * ***Description**** */
    . "<tr>"
    . "<th>Description: </th>"
    . "<td>" . $historique->description . "</td>"
    . "</tr>"
    /*     * ***Auteur**** */
    . "<tr>"
    . "<th>Auteur: </th>"
    . "<td>" . $historique->auteur . "</td>"
    . "</tr></table>";
    ?>

    <a class="btn btn-primary" href = "<?php echo $pagePrecedente; ?>">Retour</a>

</section>