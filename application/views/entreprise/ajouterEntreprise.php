<section>
    <!--Formulaire pour ajouter une entreprise -->

    <div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
    <h1>Ajouter une entreprise</h1>
    <?php
    $attributes = array('class' => 'form-horizontal');
    echo form_open('entreprise/Entreprise_c/ajouterEntreprise', $attributes);
    echo form_fieldset('Informations');
    ?>
    <div class="form-group">
        <label for="siren" class="col-sm-2 control-label">SIREN</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="siren" name="siren" placeholder="500200144" value="<?php echo set_value('siren'); ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="nom_entreprise" class="col-sm-2 control-label">Nom</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="nom_entreprise" name="nom_entreprise" placeholder="Sopra Group" value="<?php echo set_value('nom_entreprise') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="directeur" class="col-sm-2 control-label">Directeur</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="directeur" name="directeur" placeholder="Will Smith" value="<?php echo set_value('directeur') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="fax" class="col-sm-2 control-label">Fax</label>
        <div class="col-sm-10">
            <input type="tel" class="form-control" id="fax" name="fax" placeholder="0493548789" value="<?php echo set_value('fax') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="telephone" class="col-sm-2 control-label">Téléphone</label>
        <div class="col-sm-10">
            <input type="tel" class="form-control" id="telephone" name="telephone" placeholder="0665588874" value="<?php echo set_value('telephone') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" placeholder="mouhaimine@gmail.com" value="<?php echo set_value('email') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="siege" class="col-sm-2 control-label">Siege</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="pays" name="siege" placeholder="Marseille" value="<?php echo set_value('siege') ?>">
        </div>
    </div>
    <?php echo form_fieldset_close();
    echo form_fieldset('Adresse');
    ?>
    <div class="form-group">
        <label for="complementAdresse" class="col-sm-2 control-label">Complément d'adresse</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="complementAdresse" name="complementAdresse" placeholder="Résidence Montebello" value="<?php echo set_value('complementAdresse') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="numeroRue" class="col-sm-2 control-label">N° Rue</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="numeroRue" name="numeroRue" placeholder="10" value="<?php echo set_value('numeroRue') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="nomRue" class="col-sm-2 control-label">Rue</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="nomRue" name="nomRue" placeholder="montebello" value="<?php echo set_value('nomRue') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="codePostal" class="col-sm-2 control-label">Code postal</label>
        <div class="col-sm-10">
            <input type="number" class="form-control" id="codePostal" name="codePostal" placeholder="06000" value="<?php echo set_value('codePostal') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="ville" class="col-sm-2 control-label">Ville</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="ville" name="ville" placeholder="Nice" value="<?php echo set_value('ville') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="pays" class="col-sm-2 control-label">Pays</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="pays" name="pays" placeholder="France" value="<?php echo set_value('pays') ?>">
        </div>
    </div>
<?php echo form_fieldset_close(); ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success" name="valider">Valider</button>
            <button type="reset" class="btn btn-warning" name="reinitialiser">Réinitiliser</button>
            <a class="btn btn-primary" href = "<?php echo $pagePrecedente;?>">Retour</a>
        </div>
    </div>
<?php echo form_close(); ?>
</section>