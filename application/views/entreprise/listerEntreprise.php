<section>
    <form method="post" action="recherche">
        <label for="search" >Rechercher</label>
        <input type="text" name="search" placeholder="Sopra" value="<?php echo set_value('search'); ?>">
        <button type="submit" class="btn btn-default" name="valider">Valider</button>
    </form>
</section>

<section class="table-responsive">
    <!--Tableau qui liste les entreprises-->

    <?php
    echo "<h1>Liste des entreprises</h1>";
    echo "<table class='table table-striped'><tr><th>Nom</th><th>Numéro de téléphone</th><th>Email</th><th></th></tr>";
    foreach ($liste as $page):
        echo "<tr>"
        . "<td><a href = " . site_url('entreprise/Entreprise_c/afficherDetailsEntreprise?id=' . $page->siren) . ">" . $page->nom_entreprise . "</a></td>"
        . "<td>" . $page->telephone . "</td>"
        . "<td>" . $page->email . "</td>"
        //?id permet de récupérer le numéro SIREN et de traiter seulement cette entreprise.
        . "<td align=right><a class='btn btn-default ' href = " . site_url('entreprise/Entreprise_c/afficherDetailsEntreprise?id=' . $page->siren) . ">Détail</a> "
        . "<a class='btn btn-default ' href = #>Ajouter Contact</a> " //Pas encore fonctionnel
        . "</td></tr>";
    endforeach;
    echo "</table>";
    ?>
    <a class='btn btn-primary ' href = "<?php echo site_url('createpdf') ?> ">Générer un pdf</a>

</section>