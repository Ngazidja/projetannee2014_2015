<section>
    <!--Ajouter un bouton "ajouter contact"-->

    <!--Tableau qui liste les etudiants-->

    <?php
    echo "<div id='detailsEntreprise' class='table-responsive'>";
    foreach ($details->result() as $row) {
        echo "<h1>" . $row->nom_entreprise . "</h1>";
        echo "<table class='table table-striped table-hover'>"
        /*         * ***Numéro SIREN**** */
        . "<tr>"
        . "<th>SIREN: </th>"
        . "<td>" . $row->siren . "</td>"
        . "</tr>"
        /*         * ***Nom**** */
        . "<tr>"
        . "<th>Nom: </th>"
        . "<td>" . $row->nom_entreprise . "</td>"
        . "</tr>"
        /*         * ***Adresse**** */
        . "<tr>"
        . "<th>Adresse: </th>"
        . "<td>" . $row->numeroRue . " " . $row->nomRue . "<br />" . $row->complementAdresse . "<br />" . $row->codePostal . " " . $row->ville . "<br />" . $row->pays . "</td>"
        . "</tr>"
        /*         * ***Email**** */
        . "<tr>"
        . "<th>Email: </th>"
        . "<td>" . $row->email . "</td>"
        . "</tr>"
        /*         * ***Telephone**** */
        . "<tr>"
        . "<th>Téléphone: </th>"
        . "<td>" . $row->telephone . "</td>"
        . "</tr>"
        /*         * ***Fax**** */
        . "<tr>"
        . "<th>Fax: </th>"
        . "<td>" . $row->fax . "</td>"
        . "</tr>"
        /*         * ***Directeur**** */
        . "<tr>"
        . "<th>Directeur: </th>"
        . "<td>" . $row->directeur . "</td>"
        . "</tr>"
        /*         * ***Siege**** */
        . "<tr>"
        . "<th>Siège: </th>"
        . "<td>" . $row->siege . "</td>"
        . "</tr>"
        . "</table>"
        /*         * ***Ce lien permet de modifier les détails de l'entreprise**** */
        . "<a class='btn btn-primary' href = " . site_url('entreprise/Entreprise_c/formModifierEntreprise?id=' . $row->siren) . ">Modifier</a>";
    }
    ?>
    <a class="btn btn-primary" href = "<?php echo $pagePrecedente; ?>">Retour</a>
    <!--Supprimer une entreprise-->
    <?php
    if ($contrats == null && $historique == null) {
        echo "<a class='btn btn-danger' href = " . site_url('entreprise/Entreprise_c/supprimer_entreprise?id=' . $row->siren) . ">Supprimer</a>";
    }
    ?>

    <!--Affichage de la map-->
    <?php
    echo "</div>";
    //Affichage de la map
    echo "<div id='map'>";
    echo $map['html'];
    echo "</div>";
    ?>
    <!--Afficher la liste des contacts de cette boite-->
    <?php if ($contacts != null) { ?>
        <h2>Liste des contacts</h2>
        <div>
            <table class="table table-striped">
                <tr>
                    <th>Nom</th><th>Prenom</th><th>Poste</th><th>Telephone</th><th>Email</th>
                </tr>

                <?php foreach ($contacts as $row): ?>
                    <tr>
                        <td><?php echo $row->nom_contact ?></td>
                        <td><?php echo $row->prenom_contact ?></td>
                        <td><?php echo $row->poste ?></td>
                        <td><?php echo $row->telephone ?></td>
                        <td><?php echo $row->email ?></td>
                    </tr>
                <?php endforeach ?>
            </table>
        </div>
        <a class='btn btn-primary ' href = #>Ajouter Contact</a><!--Pas encore fonctionnel-->
    <?php } ?>



    <!--Afficher la liste des contrats de cette boite-->
    <?php if ($contrats != null) { ?>
        <h2>Liste des contrats</h2>
        <div>
            <table class="table table-striped">
                <tr>
                    <th>Etudiant</th><th>Type</th><th>Poste</th>
                </tr>

                <?php foreach ($contrats as $row): ?>
                    <tr>
                        <td><?php echo $row->idEtudiant ?></td>
                        <td><?php echo $row->type ?></td>
                        <td><?php echo $row->poste ?></td>
                    </tr>
                <?php endforeach ?>
            </table>
        </div>
    <?php } ?>

    <!--Affichage de l'historique de cette boite-->
    <?php if ($historique != null) { ?>
        <h2>HISTORIQUE</h2>
        <table class="table table-striped">
            <tr>
                <th>Date</th><th>Etudiant</th><th>Objet</th><th>Source</th><th>Description</th><th>Auteur</th>
            </tr>

            <?php foreach ($historique as $row): ?>
                <tr>
                    <td><?php echo $row->dateInteraction ?></td>
                    <td><?php echo $row->nom_etudiant ?></td>
                    <td><?php echo $row->objet ?></td>
                    <td><?php echo $row->source ?></td>
                    <td><?php echo $row->description ?></td>
                    <td><?php echo $row->auteur ?></td>

                </tr>
            <?php endforeach ?>
        </table>
    <?php } ?>

</section>
