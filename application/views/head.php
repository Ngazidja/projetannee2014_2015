<head>
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <title><?php echo $title; ?></title>
    <meta name="description" content="">
    
    <link rel="stylesheet" href="<?php echo css_url('bootstrap.min') ?>">
    <link href="<?php echo css_url('miage') ?>" rel="stylesheet">
    <link href="<?php echo css_url('font-awesome.min') ?>" rel="stylesheet">
    
    <!--Necessaire pour dropdown mais tout de meme bizarre-->
    <script src="<?php echo js_url('jquery.min')?>" type="text/javascript"></script>
    <script src="<?php echo js_url('bootstrap.min') ?>"></script>
    
    
    
    <?php
    if (isset($map)) {
        echo $map['js'];
    }
    ?>


</head>