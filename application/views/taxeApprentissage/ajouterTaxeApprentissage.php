<section>
    <!--Formulaire pour ajouter une taxe d'apprentissage -->

    <div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
    <h1>Ajouter une taxe d'apprentissage</h1>
    <?php
    $attributes = array('class' => 'form-horizontal');
    echo form_open('taxeApprentissage/TaxeApprentissage_c/ajouterTaxeApprentissage', $attributes);
    ?>

    <div class="form-group">
        <label for="idEntreprise" class="col-sm-2 control-label">Entreprise</label>
        <div class="col-sm-10">
            <!--<input type="number" class="form-control" id="entreprise" name="idEntreprise" placeholder="5006654" value="<?php echo set_value('idEntreprise') ?>">-->
            <!--Réussir à afficher le nom de l'entreprise-->
            <?= form_dropdown('idEntreprise', $entreprises, '', "class='form-control'"); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="annee" class="col-sm-2 control-label">Annee</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="siren" name="annee" placeholder="2014" value="<?php echo set_value('annee'); ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="montant" class="col-sm-2 control-label">Montant</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="montant" placeholder="1000" value="<?php echo set_value('montant') ?>">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success" name="valider">Valider</button>
            <button type="reset" class="btn btn-warning" name="reinitialiser">Réinitiliser</button>
            <a class="btn btn-primary" href = "<?php echo $pagePrecedente; ?>">Retour</a>
        </div>
    </div>
    <?php echo form_close(); ?>
</section>