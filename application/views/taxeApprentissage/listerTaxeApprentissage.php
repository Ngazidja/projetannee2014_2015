<section>
    <form method="post" action="recherche">
        <label for="search" >Rechercher</label>
        <input type="text" name="search" placeholder="Sopra" value="<?php echo set_value('search'); ?>">
        <button type="submit" class="btn btn-default" name="valider">Valider</button>
    </form>
</section>

<section class="table-responsive">
    <h1>  <?php echo $message; ?></h1>
    <table class="table table-striped">
        <tr>
            <th>Entreprise</th><th>Annee</th><th>Montant</th>
        </tr>

        <?php foreach ($liste as $row): ?>
            <tr>
                <td><?php echo $row->nom_entreprise ?></td>
                <td><?php echo $row->annee ?></td>
                <td><?php echo $row->montant ?> </td>
            </tr>
        <?php endforeach ?>
    </table>
</section>
