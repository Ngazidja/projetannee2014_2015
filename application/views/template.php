<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('head'); ?>

    <body>
        <!--En tête-->
        <?php $this->load->view('header'); ?>

        <!--menu-->
        
        <?php $this->load->view('nav'); ?>

        <!--Formulaire de recherche d'intéraction toujours présent-->       

        <div id="contents"><?= $contents ?></div>

        <!--Pied de page-->
        <?php $this->load->view('footer'); ?>
    </body>
</html>

