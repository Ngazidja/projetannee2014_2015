<section class="table-responsive">


    <!--Tableau qui liste les historiques-->

    <?php
    echo "<h1>" . $message . "</h1>";
    echo "<table class='table table-striped table-hover'>"
    /*     * ***Entreprise**** */
    . "<tr>"
    . "<th>Entreprise: </th>"
    . "<td>" . $contact->entreprise . "</td>"
    . "</tr>"
    /*     * ***Poste**** */
    . "<tr>"
    . "<th>Poste: </th>"
    . "<td>" . $contact->poste . "</td>"
    . "</tr>"
    /*     * ***Nom**** */
    . "<tr>"
    . "<th>Nom: </th>"
    . "<td>" . $contact->nom_contact . "</td>"
    . "</tr>"
    /*     * ***Prénom**** */
    . "<tr>"
    . "<th>Prénom: </th>"
    . "<td>" . $contact->prenom_contact . "</td>"
    . "</tr>"
    /*     * ***Téléphone**** */
    . "<tr>"
    . "<th>Téléphone: </th>"
    . "<td>" . $contact->telephone . "</td>"
    . "</tr>"
    /*     * ***Email**** */
    . "<tr>"
    . "<th>Email: </th>"
    . "<td>" . $contact->email . "</td>"
    . "</tr></table>"
    ?>

    <a class="btn btn-primary" href = "<?php echo $pagePrecedente; ?>">Retour</a>

</section>

