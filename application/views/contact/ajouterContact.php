<section>
    <div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
    <h1>Ajouter un contact</h1>
    <?php
    $attributes = array('class' => 'form-horizontal');
    echo form_open('contact/contact_c/ajouter_Contact', $attributes);
    ?>

    <?php
    $entreprise = array('name' => 'entreprise',
        'placeholder' => 'Entreprise',
        'id' => 'contactentrep',
        'class' => 'form-control',
        'value' => set_value('contactentrep')
    );
    $poste = array('name' => 'poste',
        'placeholder' => 'Poste',
        'id' => 'poste',
        'class' => 'form-control',
        'value' => set_value('poste')
    );
    $nom = array('name' => 'nom',
        'placeholder' => 'Nom',
        'id' => 'nom',
        'class' => 'form-control',
        'value' => set_value('nom')
    );
    $prenom = array('name' => 'prenom',
        'placeholder' => 'Prenom',
        'id' => 'prenom',
        'class' => 'form-control',
        'value' => set_value('prenom')
    );
    $email = array('name' => 'email',
        'placeholder' => 'Adresse e mail',
        'id' => 'email',
        'class' => 'form-control',
        'value' => set_value('email')
    );
    $portable = array('name' => 'telephone',
        'placeholder' => 'Téléphone portable',
        'id' => 'telephone',
        'class' => 'form-control',
        'value' => set_value('telephone')
    );

    $portableLabel = array('class' => "col-sm-2 control-label");
    $emailLabel = array('class' => "col-sm-2 control-label");
    $prenomLabel = array('class' => "col-sm-2 control-label");
    $nomLabel = array('class' => "col-sm-2 control-label");
    $posteLabel = array('class' => "col-sm-2 control-label");
    $entrepriseLabel = array('class' => "col-sm-2 control-label");
    ?>
    <div class="form-group">
        <?php echo form_label('Entreprise : ', '', $entrepriseLabel); ?>
        <div class="col-sm-10">
            <?= form_dropdown('entreprise', $entreprises, '', "class='form-control'"); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo form_label('Poste : ', '', $posteLabel); ?>
        <div class="col-sm-10">
            <?php echo form_input($poste); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo form_label('Nom : ', '', $nomLabel); ?>
        <div class="col-sm-10">
            <?php echo form_input($nom); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo form_label('Prenom : ', '', $prenomLabel); ?>
        <div class="col-sm-10">
            <?php echo form_input($prenom); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo form_label('Adresse e-mail : ', '', $emailLabel); ?>
        <div class="col-sm-10">
            <?php echo form_input($email); ?>
        </div>
    </div>

    <div class = "form-group">
        <?php echo form_label('Téléphone Portable : ', '', $portableLabel); ?>
        <div class="col-sm-10">
            <?php echo form_input($portable); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success" name="valider">Valider</button>
            <button type="reset" class="btn btn-warning" name="reinitialiser">Réinitiliser</button>
            <a class="btn btn-primary" href = "<?php echo $pagePrecedente; ?>">Retour</a>
        </div>
    </div>

    <?php echo form_close(); ?>
</section>