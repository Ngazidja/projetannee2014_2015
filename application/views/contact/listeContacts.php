<section>
    <form method="post" action="recherche">
        <label for="search" >Rechercher</label>
        <input type="text" name="search" placeholder="Sopra" value="<?php echo set_value('search'); ?>">
        <button type="submit" class="btn btn-default" name="valider">Valider</button>
    </form>
</section>

<section class="table-responsive">
    <h1><?= $message ?></h1>
    <table class="table table-striped">
        <tr>
            <th>Entreprise</th><th>Nom</th><th>Prenom</th><th>Poste</th><th>Telephone</th><th>Email</th>
        </tr>

        <?php foreach ($liste as $row): ?>
            <tr>
                <td><?php echo $row->entreprise ?></td>
                <td><?php echo $row->nom_contact ?></td>
                <td><?php echo $row->prenom_contact ?></td>
                <td><?php echo $row->poste ?></td>
                <td><?php echo $row->telephone ?></td>
                <td><?php echo $row->email ?></td>
            </tr>
        <?php endforeach ?>
    </table>
</section>
