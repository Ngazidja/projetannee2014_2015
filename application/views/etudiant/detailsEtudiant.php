<section class="table-responsive">
    <!--Tableau qui affiche le détail d'un étudiant-->

    <?php
    foreach ($details->result() as $row) {
        echo "<h1>" . $row->prenom_etudiant . " " . $row->nom_etudiant . "</h1>";
        echo "<table class='table table-striped table-hover'>"
        /*         * ***Numéro étudiant**** */
        . "<tr>"
        . "<th>Numéro Etudiant: </th>"
        . "<td>" . $row->num_etudiant . "</td>"
        . "</tr>"
        /*         * ***INE**** */
        . "<tr>"
        . "<th>INE: </th>"
        . "<td>" . $row->ine . "</td>"
        . "</tr>"
        /*         * ***Civilite**** */
        . "<tr>"
        . "<th>Civilite: </th>"
        . "<td>" . $row->civilite . "</td>"
        . "</tr>"
        /*         * ***Nom**** */
        . "<tr>"
        . "<th>Nom: </th>"
        . "<td>" . $row->nom_etudiant . "</td>"
        . "</tr>"
        /*         * ***Prénom**** */
        . "<tr>"
        . "<th>Prénom: </th>"
        . "<td>" . $row->prenom_etudiant . "</td>"
        . "</tr>"
        /*         * ***Date de naissance**** */
        . "<tr>"
        . "<th>Date de naissance: </th>"
        . "<td>" . $row->dateDeNaissance . "</td>"
        . "</tr>"
        /*         * ***Adresse**** */
        . "<tr>"
        . "<th>Adresse: </th>"
        . "<td>" . $row->numeroRue . " " . $row->nomRue . "<br /> " . $row->complementAdresse . "<br />" . $row->codePostal . " " . $row->ville . "<br />" . $row->pays . "</td>"
        . "</tr>"
        /*         * ***Email**** */
        . "<tr>"
        . "<th>Email: </th>"
        . "<td>" . $row->email . "</td>"
        . "</tr>"
        /*         * ***Telephone**** */
        . "<tr>"
        . "<th>Téléphone: </th>"
        . "<td>" . $row->telephone . "</td>"
        . "</tr>"
        /*         * ***Nationalité**** */
        . "<tr>"
        . "<th>Nationalité: </th>"
        . "<td>" . $row->nationalite . "</td>"
        . "</tr>"
        . "</table>"
        /*         * ***Ce lien permet de modifier les détails de l'étudiant**** */
        . "<a class='btn btn-primary' href = " . site_url('etudiant/Etudiant_c/formModifierEtudiant?id=' . $row->num_etudiant) . ">Modifier</a>";
    }
    ?>
    <a class="btn btn-primary" href = "<?php echo $pagePrecedente; ?>">Retour</a>

    <!--Supprimer etudiant-->
    <?php
    if ($tableauDeBord->result() == null && $historique == null) {
        echo "<a class='btn btn-danger' href = " . site_url('etudiant/Etudiant_c/supprimer_etudiant?id=' . $row->num_etudiant) . ">Supprimer</a>";
    }
    ?>

    <!--Affichage des stage et alternance-->
    <?php
    if ($tableauDeBord->result() != null) {
        echo "<h2> TABLEAU DE BORD </h2>";
        echo "<table class='table table-striped table-hover'><tr><th>Annee</th><th>Niveau</th><th>Entreprise</th><th>Action</th></tr>";
        foreach ($tableauDeBord->result() as $row) {
            echo "<tr>"
            . "<td>" . $row->annee . "</td>"
            . "<td>" . $row->niveau . "</td>"
            . "<td>" . $row->nom_entreprise . "</td>"
            . "<td><a class='btn btn-primary' href = " . site_url('convention/Convention_c/detailsContrat?id=' . $row->num_etudiant . '&annee=' . $row->annee) . ">Détails</a></td>"
            . "</tr><br />";
        }
        echo "</table>";
    }
    ?>

    <!--Affichage des année de scolarité-->
    <h2>Scolarité</h2>
    <table class="table table-striped table-hover">
        <tr>
            <th>Année</th><th>Cursus</th>
        </tr>
        <?php foreach ($scolarite as $row): ?>
            <tr>
                <td><?php echo $row->anneeDebut . '-' . $row->anneeFin ?></td>
                <td><?php echo $row->niveau . ' ' . $row->filiere ?></td>
            </tr>
        <?php endforeach ?>
    </table>


    <!--Affichage des interactions-->
    <?php if ($historique != null) { ?>
        <h2>HISTORIQUE</h2>
        <table class="table table-striped table-hover">
            <tr>
                <th>Date</th><th>Entreprise</th><th>Objet</th><th>Source</th><th>Description</th><th>Auteur</th>
            </tr>

            <?php foreach ($historique as $row): ?>
                <tr>
                    <td><?php echo $row->dateInteraction ?></td>
                    <td><?php echo $row->nom_entreprise ?></td>
                    <td><?php echo $row->objet ?></td>
                    <td><?php echo $row->source ?></td>
                    <td><?php echo $row->description ?></td>
                    <td><?php echo $row->auteur ?></td>

                </tr>
            <?php endforeach ?>
        </table>
    <?php } ?>

</section>