<section>
    <form method="post" action="recherche">
        <label for="search" >Rechercher</label>
        <input type="text" name="search" placeholder="Sopra" value="<?php echo set_value('search'); ?>">
        <button type="submit" class="btn btn-default" name="valider">Valider</button>
    </form>
</section>

<section class="table-responsive">
    <!--Tableau qui liste les etudiants-->

    <?php
    echo "<h1>Liste des étudiants</h1>";
    echo "<table class='table table-striped'><tr><th>Numero etudiant</th><th>Nom</th><th>Prenom</th><th></th></tr>";
    foreach ($liste as $page):
        echo "<tr>"
        . "<td><a href = " . site_url('etudiant/Etudiant_c/afficherDetailsEtudiant?id=' . $page->num_etudiant) . ">" . $page->num_etudiant . "</a></td>"
        . "<td>" . $page->nom_etudiant . "</td>"
        . "<td>" . $page->prenom_etudiant . "</td>"
        //?id permet de récupérer le numéro étudiant et de traiter seulement cet étudiant.
        . "<td align=right><a class='btn btn-default ' href = " . site_url('etudiant/Etudiant_c/afficherDetailsEtudiant?id=' . $page->num_etudiant) . ">Détail</a> "
        . "<a class='btn btn-default ' href = " . site_url('etudiant/Etudiant_c/formModifierEtudiant?id=' . $page->num_etudiant) . ">Modifier</a> "
        . "<a class='btn btn-default ' href = " . site_url('convention/Convention_c/ajouterContrat?id=') . $page->num_etudiant . ">Ajouter un contrat</a></td>"
        . "</tr>";
    endforeach;
    echo "</table>";
    ?>

</section>