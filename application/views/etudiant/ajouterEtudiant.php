<section>
    <!--Formulaire pour ajouter un étudiant -->

    <div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
    <h1>Ajouter un étudiant</h1>
    <?php
    $attributes = array('class' => 'form-horizontal');
    echo form_open('etudiant/Etudiant_c/ajouterEtudiant', $attributes);
    echo form_fieldset('Informations personnelles');
    ?>
    <div class="form-group">
        <label for="num_etudiant" class="col-sm-2 control-label">Numéro Etudiant</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="num_etudiant" name="num_etudiant" placeholder="sw002558" value="<?php echo set_value('num_etudiant'); ?>" >
        </div>
    </div>

    <div class="form-group">
        <label for="ine" class="col-sm-2 control-label">INE</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="ine" name="ine" placeholder="INE" value="<?php echo set_value('ine') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="civilite" class="col-sm-2 control-label">Civilité</label>
        <div class="col-sm-10">
            <div class="radio">
                <label>
                    <input type="radio" name="civilite" id="optionsRadios1" value="monsieur" checked>
                    Monsieur
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="civilite" id="optionsRadios2" value="madame">
                    Madame
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="nom_etudiant" class="col-sm-2 control-label">Nom</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="nom_etudiant" name="nom_etudiant" placeholder="Smith" value="<?php echo set_value('nom_etudiant') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="prenom_etudiant" class="col-sm-2 control-label">Prenom</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="prenom_etudiant" name="prenom_etudiant" placeholder="Will" value="<?php echo set_value('prenom_etudiant') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="dateNaissance" class="col-sm-2 control-label">Date de naissance</label>
        <div class="col-sm-10">
            <input type="date" class="form-control" id="dateNaissance" name="dateNaissance" placeholder="02/05/1993" value="<?php echo set_value('dateDeNaissance') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="telephone" class="col-sm-2 control-label">Téléphone</label>
        <div class="col-sm-10">
            <input type="tel" class="form-control" id="telephone" name="telephone" placeholder="0665588874" value="<?php echo set_value('telephone') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" placeholder="mouhaimine@gmail.com" value="<?php echo set_value('email') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="nationalité" class="col-sm-2 control-label">Nationalité</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="nationalité" name="nationalite" placeholder="Comorienne" value="<?php echo set_value('nationalite') ?>">
        </div>
    </div>

    <?php
    echo form_fieldset_close();
    echo form_fieldset('Adresse');
    ?>
    <div class="form-group">
        <label for="complementAdresse" class="col-sm-2 control-label">Complément d'adresse</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="complementAdresse" name="complementAdresse" placeholder="Résidence Montebello" value="<?php echo set_value('complementAdresse') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="numeroRue" class="col-sm-2 control-label">N° Rue</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="numeroRue" name="numeroRue" placeholder="10" value="<?php echo set_value('numeroRue') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="nomRue" class="col-sm-2 control-label">Rue</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="nomRue" name="nomRue" placeholder="montebello" value="<?php echo set_value('nomRue') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="codePostal" class="col-sm-2 control-label">Code postal</label>
        <div class="col-sm-10">
            <input type="number" class="form-control" id="codePostal" name="codePostal" placeholder="06000" value="<?php echo set_value('codePostal') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="ville" class="col-sm-2 control-label">Ville</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="ville" name="ville" placeholder="Nice" value="<?php echo set_value('ville') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="pays" class="col-sm-2 control-label">Pays</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="pays" name="pays" placeholder="France" value="<?php echo set_value('pays') ?>">
        </div>
    </div>
    <?php
    echo form_fieldset_close();
    echo form_fieldset('Scolarité');
    ?>

    <div class="form-group">
        <label for="anneeDebut" class="col-sm-2 control-label">De</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="anneeDebut" placeholder="2013" value="<?php echo set_value('anneeDebut') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="anneeFin" class="col-sm-2 control-label">À</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="anneeFin" placeholder="2014" value="<?php echo set_value('anneeFin') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="niveau" class="col-sm-2 control-label">Niveau D'étude</label>
        <div class="col-sm-10">
            <select name = "niveau" id = "niveau" class="form-control" >
                <option selected="selected">Licence 3</option>
                <option>Master 1</option>
                <option>Master 2</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="filiere" class="col-sm-2 control-label">Filière</label>
        <div class="col-sm-10">
            <select name = "filiere" id = "filiere" class="form-control">
                <option selected="selected">MIAGE</option>
                <option>NTDP</option>
                <option>MBDS</option>
                <option>SD</option>
            </select>
        </div>
    </div>

    <?php echo form_fieldset_close(); ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success" name="valider">Valider</button>
            <button type="reset" class="btn btn-warning" name="reinitialiser">Réinitiliser</button>
            <a class="btn btn-primary" href = "<?php echo $pagePrecedente; ?>">Retour</a>
        </div>
    </div>
    <?php echo form_close(); ?>
</section>