<section>
    <!--Formulaire pour ajouter une entreprise -->

    <div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
    <?php
    $attributes = array('class' => 'form-horizontal');
    echo form_open('convention/Convention_c/ajouterContrat?id=' . $id, $attributes);
    echo form_fieldset('Ajouter un contrat');
    ?>

    <div class="form-group">
        <label for="idEtudiant" class="col-sm-2 control-label">Etudiant</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="etudiant" name="idEtudiant" placeholder="az005896" value="<?php echo $id ?>" readonly>
        </div>
    </div>
    <!--Je dois pouvoir selectionner parmi les entreprises existante-->
    <div class="form-group">
        <label for="idEntreprise" class="col-sm-2 control-label">Entreprise</label>
        <div class="col-sm-10">
            <!--<input type="number" class="form-control" id="entreprise" name="idEntreprise" placeholder="5006654" value="<?php echo set_value('idEntreprise') ?>">-->
            <!--Réussir à afficher le nom de l'entreprise-->
            <?= form_dropdown('idEntreprise', $entreprises, '', "class='form-control' id='entreprise'"); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="type" class="col-sm-2 control-label">Type du contrat</label>
        <div class="col-sm-10">
            <div class="radio">
                <label>
                    <input type="radio" name="type" value="stage" checked>
                    Stage
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="type" value="alternance">
                    Alternance
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="alternace" class="col-sm-2 control-label">Type d'alternance</label>
        <div class="col-sm-10">
            <div class="radio">
                <label>
                    <input type="radio" name="alternance" value="apprentissage" checked>
                    Apprentissage
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="alternance" value="professionnalisation">
                    Professionnalisation
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="poste" class="col-sm-2 control-label">Poste</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="poste" name="poste" placeholder="Développeur web" value="<?php echo set_value('poste') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="fax" class="col-sm-2 control-label">Année</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="annee" name="annee" placeholder="2014" value="<?php echo set_value('annee') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="duree" class="col-sm-2 control-label">Durée (en mois)</label>
        <div class="col-sm-10">
            <input type="number" class="form-control" id="duree" name="duree" placeholder="3" value="<?php echo set_value('duree') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="remuneration" class="col-sm-2 control-label">Rémunération</label>
        <div class="col-sm-10">
            <input type="number" class="form-control" id="remunaration" name="remuneration" placeholder="501" value="<?php echo set_value('remuneration') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="tuteurEntreprise" class="col-sm-2 control-label">Tuteur enteprise</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="tuteurEntreprise" name="tuteurEntreprise" placeholder="Will Smith" value="<?php echo set_value('tuteurEntreprise') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="tuteurPedagogique" class="col-sm-2 control-label">Tuteur Pédagogique</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="tuteurPedagogique" name="tuteurPedagogique" placeholder="Jean Marc Fedou" value="<?php echo set_value('tuteurPedagogique') ?>">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default" name="valider">Valider</button>
            <button type="reset" class="btn btn-warning" name="reinitialiser">Réinitiliser</button>
            <a class="btn btn-primary" href = "<?php echo $pagePrecedente; ?>">Retour</a>
        </div>
    </div>
    <?php echo form_close(); ?>
</section>