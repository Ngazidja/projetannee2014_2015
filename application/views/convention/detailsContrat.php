<section class="table-responsive">
    <!--Tableau qui affiche le détail d'un étudiant-->

    <?php
    foreach ($contrat->result() as $row) {
        echo "<h1>Détails contrat</h1>";
        echo "<table class='table table-striped table-hover'>"
        /*         * ***Etudiant**** */
        . "<tr>"
        . "<th>Etudiant: </th>"
        . "<td>" . $row->prenom_etudiant . " " . $row->nom_etudiant . "</td>"
        . "</tr>"
        /*         * ***Entreprise**** */
        . "<tr>"
        . "<th>Entreprise: </th>"
        . "<td>" . $row->nom_entreprise . "</td>"
        . "</tr>"
        /*         * ***Poste**** */
        . "<tr>"
        . "<th>Poste: </th>"
        . "<td>" . $row->poste . "</td>"
        . "</tr>"
        /*         * ***Sujet**** */
        . "<tr>"
        . "<th>Sujet: </th>"
        . "<td>" . $row->sujet . "</td>"
        . "</tr>"
        /*         * ***Annee**** */
        . "<tr>"
        . "<th>Année: </th>"
        . "<td>" . $row->annee . "</td>"
        . "</tr>"
        /*         * ***Durée**** */
        . "<tr>"
        . "<th>Durée: </th>"
        . "<td>" . $row->duree . " mois </td>"
        . "</tr>"
        /*         * ***Rémunération**** */
        . "<tr>"
        . "<th>Rémunération: </th>"
        . "<td>" . $row->remuneration . " € </td>"
        . "</tr>"
        /*         * ***Tuteur entreprise**** */
        . "<tr>"
        . "<th>Tuteur entreprise: </th>"
        . "<td>" . $row->tuteurEntreprise . "</td>"
        . "</tr>"
        /*         * ***Tuteur pédagogique**** */
        . "<tr>"
        . "<th>Tuteur pédagogique: </th>"
        . "<td>" . $row->tuteurPedagogique . "</td>"
        . "</tr>"

        /*         * ***Type de contrat**** */
        . "<tr>"
        . "<th>Type de contrat: </th>"
        . "<td>" . $row->type . "</td>"
        . "</tr>"
        /*         * ***Type d'alternance**** */
        . "<tr>"
        . "<th>Type d'alternance: </th>"
        . "<td>" . $row->alternance . "</td>"
        . "</tr>"
        . "</table>";
    }
    ?>
    <a class="btn btn-primary" href = "<?php echo $pagePrecedente; ?>">Retour</a>
</section>