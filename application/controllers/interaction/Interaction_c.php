<?php

class Interaction_c extends CI_Controller {

    public function __construct() {
        parent:: __construct();

//Chargement des ressources pour tout le contrôleur
        $this->load->database();

//  Chargement du modèle de gestion des etudiants
        $this->load->model('etudiant/etudiant_m');
        $this->load->model('entreprise/entreprise_m');
        $this->load->model('interaction/interaction_m');
    }

    public function index() {
        $this->accueil();
    }

    /*     * ****************Accueil**************************************** */

    public function accueil() {
        $data['title'] = 'CRM Miage';
        $this->template->load('template', 'accueil/crm_accueil', $data);
    }

    public function ajouter_une_interaction() {

        $this->form_validation->set_rules('objet', 'objet', 'required');
        $this->form_validation->set_rules('dateinteraction', 'date de l interaction', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');

        if ($this->form_validation->run() == TRUE) {
            $this->interaction_m->inserer_interaction();
            redirect('interaction/interaction_c/ajouter_une_interaction');
        } else {
            $listeEtudiants = $this->etudiant_m->getEtudiant();
            $listeEntreprises = $this->entreprise_m->getEntreprise();

            $etudiants = array();
            $entreprises = array();

            foreach ($listeEtudiants as $etudiant) {
                $etudiants[$etudiant->num_etudiant] = $etudiant->etudiant;
            }

            foreach ($listeEntreprises as $entreprise) {
                $entreprises[$entreprise->siren] = $entreprise->nom_entreprise;
            }

            $data = array('title' => 'Ajouter une interaction', 'etudiants' => $etudiants,
                'entreprises' => $entreprises, 'pagePrecedente' => $_SERVER['HTTP_REFERER']);

            $this->template->load('template', 'interaction/ajouterInteraction', $data);
        }
    }

    public function afficher_historique() {
        $historique = $this->interaction_m->afficherHistorique();
        $data = array('historique' => $historique,
            'message' => 'Historique des interactions ', 'title' => 'Liste des historiques');
        $this->template->load('template', 'interaction/listeInteractions', $data);
    }

    public function detail_historique($id) {

        if ($this->input->post('retour')) {
            // on revient dans la liste des étudiants
            redirect('interaction/interaction_c/afficher_historique');
        } else {
            //on récupère la description de l'historique dont l'id = $id
            $historique = $this->interaction_m->detailHistorique($id);

            $data = array('id' => $id,
                "historique" => $historique,
                'message' => 'Detail de l\'interaction', 'title' => 'Détail d\'une interaction',
                'pagePrecedente' => $_SERVER['HTTP_REFERER']);
            $this->template->load('template', 'interaction/detailHistorique', $data);
        }
    }

    public function recherche() {
        $interaciton = $this->input->post('search');
        $interactions = $this->interaction_m->recherche($interaciton);
//        echo $interactions->num_rows();
        if ($interactions == null) {
            $data = array('historique' => $interactions, 'title' => 'Recherche interaction', 'recherche' => 'interaction');
            $this->template->load('template', 'rechercheNull', $data);
        } else {
            $data = array('historique' => $interactions, 'message' => 'Liste des interactions', 'title' => 'Recherche interaction');
            $this->template->load('template', 'interaction/listeInteractions', $data);
        }
    }

}
