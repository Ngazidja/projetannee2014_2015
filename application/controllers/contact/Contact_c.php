<?php

class Contact_c extends CI_controller {

    public function __construct() {
        parent:: __construct();

        $this->load->database();
        $this->load->model('contact/contact_m');
        $this->load->model('entreprise/entreprise_m');
    }

    public function index() {
        $this->accueil();
    }

    /*     * ****************Accueil**************************************** */

    public function accueil() {
        $data['title'] = 'CRM Miage';
        $this->template->load('template', 'accueil/crm_accueil', $data);
    }

    public function afficher_Les_Contacts() {
        $contact = $this->contact_m->afficherTousLesContacts();
        $data = array('liste' => $contact, 'message' => 'Liste des contacts', 'title' => 'Liste des contacts');
        $this->template->load('template', 'contact/listeContacts', $data);
    }

    public function recherche() {
        $nom_contact = $this->input->post('search');
        $contact = $this->contact_m->recherche($nom_contact);
        if ($contact == null) {
            $data = array('title' => 'Liste des contacts', 'recherche' => 'contact');
            $this->template->load('template', 'rechercheNull', $data);
        } else {
            $data = array('liste' => $contact, 'message' => 'Liste des contacts', 'title' => 'Liste des contacts');
            $this->template->load('template', 'contact/listeContacts', $data);
        }
    }

    public function ajouter_Contact() {

//        $data['entreprises'] = $this->entreprise_m->get_dropdown_entreprises();

        $this->form_validation->set_rules('nom', 'contactentrep', 'required');
        $this->form_validation->set_rules('prenom', 'Prenom ', 'required');
        $this->form_validation->set_rules('email', 'e-mail', 'required|valid_email');

        if ($this->form_validation->run() == TRUE) {
            $this->contact_m->inserer_Etudiant();

            redirect('contact/contact_c/afficher_Les_Contacts');
        } else {
            $data = array('title' => 'Ajouter un contact', 'pagePrecedente' => $_SERVER['HTTP_REFERER'],
                'entreprises' => $this->entreprise_m->get_dropdown_entreprises());
            $this->template->load('template', 'contact/ajouterContact', $data);
        }
    }

    public function detail_Contact() {

        $contact = $this->contact_m->detailContact($_GET['id']);
        $data = array('contact' => $contact, 'message' => 'Detail Contact', 'pagePrecedente' => $_SERVER['HTTP_REFERER']);
        $this->template->load('template', 'contact/detailsContact', $data);
    }

}
