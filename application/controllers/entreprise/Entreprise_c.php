<?php

class Entreprise_c extends CI_Controller {

    public function __construct() {
        parent:: __construct();

//Chargement des ressources pour tout le contrôleur
        $this->load->database();

//  Chargement du modèle de gestion des etudiants
        $this->load->model('entreprise/entreprise_m');
        $this->load->model('contact/contact_m');
        $this->load->model('convention/convention_m');
        $this->load->model('interaction/interaction_m');
    }

    public function index() {
        $this->accueil();
    }

    /*     * ****************Accueil**************************************** */

    public function accueil() {
        $data['title'] = 'CRM Miage';
        $this->template->load('template', 'accueil/crm_accueil', $data);
    }

    public function ajouterEntreprise() {
        $data['pagePrecedente'] = $_SERVER['HTTP_REFERER'];

        $nom_contact = $this->input->post('siren');
        echo $nom_contact;
        $data['title'] = 'Ajouter une entreprise';
        /*         * Chargement des méthodes si déclarées dans le contrôleur* */
        $this->form_validation->set_rules('siren', '"identifiant"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('nom_entreprise', '"Nom"', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('adresse', '"adresse"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('telephone', '"téléphone"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', '"mail"', 'trim|required|valid_email|is_unique[etudiant.email]|xss_clean');

        if ($this->form_validation->run()) {

//  Sauvegarde de l'entreprise dans la base de données
            $this->entreprise_m->ajouter_entreprise($this->input->post('siren'), $this->input->post('nom_entreprise'), $this->input->post('complementAdresse'), $this->input->post('telephone'), $this->input->post('email'), $this->input->post('directeur'), $this->input->post('fax'), $this->input->post('numeroRue'), $this->input->post('nomRue'), $this->input->post('codePostal'), $this->input->post('ville'), $this->input->post('pays'), $this->input->post('siege'));

            $data['entreprise'] = array($this->input->post('nom_entreprise'));
//  Affichage de la confirmation
            $this->template->load('template', 'entreprise/confirmation', $data);
        } else {
            $this->template->load('template', 'entreprise/ajouterEntreprise', $data);
        }
    }

    public function listerEntreprise() {
        $data['title'] = 'Lister les entreprises';
        $data['liste'] = $this->entreprise_m->liste_entreprise();
//        echo current_url();
//  On charge la vue
        $this->template->load('template', 'entreprise/listerEntreprise', $data);
    }

    public function confirmationAjouterEntreprise() {
        $data['title'] = 'Confirmation';
        $this->template->load('template', 'entreprise/confirmation', $data);
    }

    public function afficherDetailsEntreprise() {
        $data['pagePrecedente'] = $_SERVER['HTTP_REFERER'];

        $data['title'] = 'Détails de l\'entreprise';

        //Charge la map et crée les données de la map.
        $adresse = $this->entreprise_m->getAdresseEntreprise($_GET['id']);
        $entreprise = $this->entreprise_m->getNomEntreprise($_GET['id']);
        $config = array();
        $config['center'] = $adresse->ville . ', ' . $adresse->pays;
        $this->googlemaps->initialize($config);

        $marker = array();
        $marker['position'] = $adresse->nomRue . ' ' . $adresse->nomRue . ' ' . $adresse->codePostal . ' ' . $adresse->ville . ', ' . $adresse->pays;
        $marker['infowindow_content'] = $entreprise->nom_entreprise;
        $this->googlemaps->add_marker($marker);

        $data['map'] = $this->googlemaps->create_map();

        if (isset($_GET['id'])) {
            $data['details'] = $this->entreprise_m->details_entreprise($_GET['id']);
            $data['contacts'] = $this->contact_m->contactsEntrepise($_GET['id']);
            $data['contrats'] = $this->convention_m->contratsEntreprise($_GET['id']);
            $data['historique'] = $this->interaction_m->historiqueEntreprise($_GET['id']);

//  On charge la vue
            $this->template->load('template', 'entreprise/detailsEntreprise', $data);
        }
    }

//Cette méthode ne fait qu'afficher un formulaire avec les données de l'étudiant
    public function formModifierEntreprise() {
        $data['title'] = 'Modifier une entreprise';
        if (isset($_GET['id'])) {
            $data['modifier'] = $this->entreprise_m->details_entreprise($_GET['id']);
            $this->template->load('template', 'entreprise/modifierEntreprise', $data);
        }
    }

//Cette méthode modifie l'entreprise dans la base de données
    public function modifierEntreprise() {
        $data['pagePrecedente'] = $_SERVER['HTTP_REFERER'];

        $data['title'] = 'Confirmation de modification';
        /*         * Chargement des méthodes si déclarées dans le contrôleur* */
        $this->form_validation->set_rules('siren', '"identifiant"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('nom_entreprise', '"nom"', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('adresse', '"adresse"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('telephone', '"téléphone"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', '"mail"', 'trim|required|valid_email|is_unique[etudiant.email]|xss_clean');

        if ($this->form_validation->run()) {
            $this->entreprise_m->modifier_entreprise($this->input->post('siren'), $this->input->post('nom_entreprise'), $this->input->post('complementAdresse'), $this->input->post('telephone'), $this->input->post('email'), $this->input->post('directeur'), $this->input->post('fax'), $this->input->post('numeroRue'), $this->input->post('nomRue'), $this->input->post('codePostal'), $this->input->post('ville'), $this->input->post('pays'), $this->input->post('siege'));
            $data['entreprise'] = array($this->input->post('nom'));
            $this->template->load('template', 'entreprise/confirmationModification', $data);
        } else {
            $this->formModifierEntreprise();
        }
    }

    public function recherche() {
        
        $nom_entreprise = $this->input->post('search');
        $entreprise = $this->entreprise_m->recherche($nom_entreprise);
        if ($entreprise == null) {
            $data = array('title' => 'Recherche interaction', 'recherche' => 'entreprise');
            $this->template->load('template', 'rechercheNull', $data);
        } else {
            $data = array('liste' => $entreprise, 'message' => 'Liste des entreprises', 'title' => 'Recherche entreprise');
            $this->template->load('template', 'entreprise/listerEntreprise', $data);
        }
    }

    public function supprimer_entreprise() {
        $this->entreprise_m->supprimer_entreprise($_GET['id']);
        $this->listerEntreprise();
    }

}
