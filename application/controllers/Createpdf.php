<?php

class Createpdf extends CI_Controller {

    public function __construct() {
        parent:: __construct();
        $this->load->database();
        $this->load->model('entreprise/entreprise_m');
    }

    public function index() {
        $data['liste'] = $this->entreprise_m->liste_entreprise();



        $this->load->library('html2pdf', array('P','A4','fr'));
//        $this->html2pdf->html($this->load->view('entreprise/listerEntreprise', $data, true));
//        if ($this->html2pdf->create('download')) {
//            //PDF was successfully saved or downloaded
//            echo 'PDF saved';
//        }
        $this->html2pdf->WriteHTML($this->load->view('entreprise/pdf', $data, true));
        $this->html2pdf->Output('listeEntreprises.pdf');
    }

}
