<?php

class Etudiant_c extends CI_Controller {

    public function __construct() {
        parent:: __construct();

//Chargement des ressources pour tout le contrôleur
        $this->load->database();

//  Chargement du modèle de gestion des etudiants
        $this->load->model('etudiant/etudiant_m');
        $this->load->model('entreprise/entreprise_m');
        $this->load->model('interaction/interaction_m');
    }

    public function index() {
        $this->accueil();
    }

    /*     * ****************Accueil**************************************** */

    public function accueil() {
        $data['title'] = 'CRM Miage';
        $this->template->load('template', 'accueil/crm_accueil', $data);
    }

    public function ajouterEtudiant() {
        $data['pagePrecedente'] = $_SERVER['HTTP_REFERER'];

        $data['title'] = 'Ajouter un étudiant';

        /*         * Chargement des méthodes si déclarées dans le contrôleur* */
        $this->form_validation->set_rules('num_etudiant', '"identifiant"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('nom_etudiant', '"nom"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('prenom_etudiant', '"prénom"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('telephone', '"téléphone"', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('reg[dateNaissance]', 'Date of birth', 'regex_match[(0[1-9]|1[0-9]|2[0-9]|3(0|1))-(0[1-9]|1[0-2])-\d{4}]');
        $this->form_validation->set_rules('niveau', '"niveau"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', '"mail"', 'trim|required|valid_email|is_unique[etudiant.email]|xss_clean');

        if ($this->form_validation->run()) {

//  Sauvegarde de l'etudiant dans la base de données
            $this->etudiant_m->ajouter_etudiant($this->input->post('num_etudiant'), $this->input->post('nom_etudiant'), $this->input->post('prenom_etudiant'), $this->input->post('dateDeNaissance'), $this->input->post('telephone'), $this->input->post('email'), $this->input->post('ine'), $this->input->post('civilite'), $this->input->post('complementAdresse'), $this->input->post('numeroRue'), $this->input->post('nomRue'), $this->input->post('codePostal'), $this->input->post('ville'), $this->input->post('pays'), $this->input->post('nationalite'));
            $this->etudiant_m->ajouterScolariteEtudiant($this->input->post('num_etudiant'), $this->input->post('anneeDebut'), $this->input->post('anneeFin'), $this->input->post('niveau'), $this->input->post('filiere'));
            $data['etudiant'] = array($this->input->post('nom'), $this->input->post('prenom'));
//  Affichage de la confirmation
            $this->template->load('template', 'etudiant/confirmation', $data);
        } else {
            $this->template->load('template', 'etudiant/ajouterEtudiant', $data);
        }
    }

    public function confirmationAjouterEtudiant() {
        $data['title'] = 'Confirmation';
        $this->template->load('template', 'etudiant/confirmation', $data);
    }

    public function listerEtudiant() {

        $data['title'] = 'Liste des étudiants';
        $data['liste'] = $this->etudiant_m->liste_etudiant();
//  On charge la vue avec le template
        $this->template->load('template', 'etudiant/listerEtudiant', $data);
    }

    public function afficherDetailsEtudiant() {
        $data['pagePrecedente'] = $_SERVER['HTTP_REFERER'];

        $data['title'] = 'Détails de l\'étudiants';
        if (isset($_GET['id'])) {
            $data['details'] = $this->etudiant_m->details_etudiant($_GET['id']);
            $data['tableauDeBord'] = $this->etudiant_m->tableauDeBord($_GET['id']);
            $data['historique'] = $this->interaction_m->historiqueEtudiant($_GET['id']);
            $data['scolarite'] = $this->etudiant_m->scolarite($_GET['id']);
//  On charge la vue avec le template
            $this->template->load('template', 'etudiant/detailsEtudiant', $data);
        }
    }

//Cette méthode ne fait qu'afficher un formulaire avec les données de l'étudiant
    public function formModifierEtudiant() {
        $data['title'] = 'Modifier un étudiant';
        if (isset($_GET['id'])) {
            $data['modifier'] = $this->etudiant_m->details_etudiant($_GET['id']);
            $this->template->load('template', 'etudiant/modifierEtudiant', $data);
        }
    }

//Cette méthode modifie l'étudiant dans la base de données
    public function modifierEtudiant() {
        $data['pagePrecedente'] = $_SERVER['HTTP_REFERER'];

        $data['title'] = 'Confirmation de modification';

        /*         * Chargement des méthodes si déclarées dans le contrôleur* */
        $this->form_validation->set_rules('num_etu', '"L\'identifiant"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('nom', '"Le Nom"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('prenom', '"Le prénom"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('telephone', '"Le téléphone"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('reg[dateNaissance]', 'Date of birth', 'regex_match[(0[1-9]|1[0-9]|2[0-9]|3(0|1))-(0[1-9]|1[0-2])-\d{4}]');
        $this->form_validation->set_rules('niveau', '"Le niveau"', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', '"Le mail"', 'trim|required|valid_email|is_unique[etudiant.email]|xss_clean');

        if ($this->form_validation->run()) {
            $this->etudiant_m->modifier_etudiant($this->input->post('num_etu'), $this->input->post('nom'), $this->input->post('prenom'), $this->input->post('dateNaissance'), $this->input->post('telephone'), $this->input->post('email'), $this->input->post('niveau'));
            $data['etudiant'] = array($this->input->post('nom'), $this->input->post('prenom'));
            $this->template->load('template', 'etudiant/confirmationModification', $data);
        } else {
            $this->formModifierEtudiant();
        }
    }

    public function recherche() {

        $nom_etudiant = $this->input->post('search');
        $etudiant = $this->etudiant_m->recherche($nom_etudiant);
        if ($etudiant == null) {
            $data = array('message' => 'Liste des étudiants', 'title' => 'Liste des étudiants', 'recherche' => 'étudiant');
            $this->template->load('template', 'rechercheNull', $data);
        } else {
            $data = array('liste' => $etudiant, 'message' => 'Liste des étudiant', 'title' => 'Recherche etudiant');
            $this->template->load('template', 'etudiant/listerEtudiant', $data);
        }
    }

    public function supprimer_etudiant() {
        $this->etudiant_m->supprimer_etudiant($_GET['id']);
        $this->listerEtudiant();
    }

}
