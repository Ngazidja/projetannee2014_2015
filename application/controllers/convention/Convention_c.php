<?php

class Convention_c extends CI_Controller {

    public function __construct() {
        parent:: __construct();

//Chargement des ressources pour tout le contrôleur
        $this->load->database();

//  Chargement du modèle de gestion des etudiants
        $this->load->model('convention/convention_m');
        $this->load->model('entreprise/entreprise_m');
    }

    public function index() {
        $this->accueil();
    }

    /*     * ****************Accueil**************************************** */

    public function accueil() {
        $data['title'] = 'CRM Miage';
        $this->template->load('template', 'accueil/crm_accueil', $data);
    }

    /*
     * Ajoute une convention ou un contrat d'alternance
     */

    public function ajouterContrat() {
        $data['pagePrecedente'] = $_SERVER['HTTP_REFERER'];

        $this->template->set('title', 'Ajouter un contrat');
        $data['entreprises'] = $this->entreprise_m->get_dropdown_entreprises();

        /*         * Chargement des méthodes si déclarées dans le contrôleur* */
        $this->form_validation->set_rules('idEntreprise', 'entreprise', 'required');
        $this->form_validation->set_rules('idEtudiant', 'etudiant', 'required');

        if ($this->form_validation->run()) {

//  Sauvegarde de l'etudiant dans la base de données
            $this->convention_m->ajouter_contrat($_GET['id'], $this->input->post('idEntreprise'), $this->input->post('poste'), $this->input->post('annee'), $this->input->post('duree'), $this->input->post('remuneration'), $this->input->post('tuteurEntreprise'), $this->input->post('tuteurPedagogique'), $this->input->post('type'), $this->input->post('alternance'));
            $this->template->load('template', 'convention/confirmation');
        } else {
            $data['id'] = $_GET['id'];
            $this->template->load('template', 'convention/ajouterContrat', $data);
        }
    }

    /*
     * Retourne le détail d'un contrat.
     * Cette fonction n'est pas au point car elle renvoie le détail de toutes les conventions (contrat)
     */

    public function detailsContrat() {
        $data['pagePrecedente'] = $_SERVER['HTTP_REFERER'];

        $data['title'] = 'Détails du contrat';
        $data['contrat'] = $this->convention_m->detailsContrat($_GET['id'], $_GET['annee']);
        $this->template->load('template', 'convention/detailsContrat', $data);
    }

    public function recherche() {
        $nom_contrat = $this->input->post('search');
        $contrat = $this->contact_m->recherche($nom_contrat);
        if ($contrat == null) {
            $data = array('title' => 'Liste des étudiants', 'recherche' => 'contrat');
            $this->template->load('template', 'rechercheNull', $data);
        } else {
            $data = array('liste' => $contrat, 'message' => 'Liste des contrats', 'title' => 'Liste des contrats');
            $this->template->load('template', 'contrat', $data); //Page non existante
        }
    }

}
