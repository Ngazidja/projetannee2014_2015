<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Controlleur
 */

class Accueil_c extends CI_Controller {

    public function __construct() {
        parent:: __construct();

//Chargement des ressources pour tout le contrôleur
        $this->load->database();
        $this->load->model('accueil_m');
        $this->load->model('entreprise/entreprise_m');
        $this->load->model('contact/contact_m');
        $this->load->model('convention/convention_m');
        $this->load->model('interaction/interaction_m');
        $this->load->model('etudiant/etudiant_m');
    }

    public function index() {

        $this->accueil();
    }

    /*     * ****************Accueil**************************************** */

    public function accueil() {
        $data['title'] = 'CRM Miage';
        $this->template->load('template', 'accueil/crm_accueil', $data);
    }

    public function rechercheEtudiant() {
        $etudiant = $this->input->post('rechercheEtudiant');
        $etudiants = $this->accueil_m->rechercheEtudiant($etudiant);

        if ($etudiants == null) {
            $data = array('title' => 'Recherche historique étudiant', 'recherche' => 'etudiant');
            $this->template->load('template', 'rechercheNull', $data);
        } else {

            $this->afficherDetailsEtudiant($etudiants->row()->num_etudiant);
        }
    }

    public function rechercheEntreprise() {
        $entreprise = $this->input->post('rechercheEntreprise');
        $entreprises = $this->accueil_m->rechercheEntreprise($entreprise);

        if ($entreprises == null) {
            $data = array('title' => 'Recherche historique entreprise', 'recherche' => 'entreprise');
            $this->template->load('template', 'rechercheNull', $data);
        } else {
            $this->afficherDetailsEntreprise($entreprises->row()->siren);
        }
    }

    public function afficherDetailsEntreprise($id) {
        $data['pagePrecedente'] = $_SERVER['HTTP_REFERER'];

        $data['title'] = 'Détails de l\'entreprise';

//Charge la map et crée les données de la map.
        $adresse = $this->entreprise_m->getAdresseEntreprise($id);
        $entreprise = $this->entreprise_m->getNomEntreprise($id);
        $config = array();
        $config['center'] = $adresse->ville . ', ' . $adresse->pays;
        $this->googlemaps->initialize($config);

        $marker = array();
        $marker['position'] = $adresse->nomRue . ' ' . $adresse->nomRue . ' ' . $adresse->codePostal . ' ' . $adresse->ville . ', ' . $adresse->pays;
        $marker['infowindow_content'] = $entreprise->nom_entreprise;
        $this->googlemaps->add_marker($marker);

        $data['map'] = $this->googlemaps->create_map();

        $data['details'] = $this->entreprise_m->details_entreprise($id);
        $data['contacts'] = $this->contact_m->contactsEntrepise($id);
        $data['contrats'] = $this->convention_m->contratsEntreprise($id);
        $data['historique'] = $this->interaction_m->historiqueEntreprise($id);

//  On charge la vue
        $this->template->load('template', 'entreprise/detailsEntreprise', $data);
    }

    public function afficherDetailsEtudiant($etudiant) {
        $data['pagePrecedente'] = $_SERVER['HTTP_REFERER'];

        $data['title'] = 'Détails de l\'étudiants';

        $data['details'] = $this->etudiant_m->details_etudiant($etudiant);
        $data['tableauDeBord'] = $this->etudiant_m->tableauDeBord($etudiant);
        $data['historique'] = $this->interaction_m->historiqueEtudiant($etudiant);
        $data['scolarite'] = $this->etudiant_m->scolarite($etudiant);
//  On charge la vue avec le template
        $this->template->load('template', 'etudiant/detailsEtudiant', $data);
    }

}
