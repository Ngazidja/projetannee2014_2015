<?php

class TaxeApprentissage_c extends CI_Controller {

    public function __construct() {
        parent:: __construct();

//Chargement des ressources pour tout le contrôleur
        $this->load->database();

        $this->load->model('taxeApprentissage/taxe_apprentissage_m');
        $this->load->model('contact/contact_m');
        $this->load->model('entreprise/entreprise_m');
    }

    public function index() {
        $this->accueil();
    }

    /*     * ****************Accueil**************************************** */

    public function accueil() {

        $data['title'] = 'CRM Miage';
        $this->template->load('template', 'accueil/crm_accueil', $data);
    }

    public function ajouterTaxeApprentissage() {
        // $data['entreprises'] = $this->entreprise_m->get_dropdown_entreprises();

        $this->form_validation->set_rules('idEntreprise', 'entreprise', 'required');
        $this->form_validation->set_rules('annee', 'annee', 'required');
        $this->form_validation->set_rules('montant', 'montant', 'required');

        if ($this->form_validation->run() == TRUE) {
            $this->taxe_apprentissage_m->ajouterTaxeApprentissage();
            redirect('taxeApprentissage/taxeApprentissage_c/ajouterTaxeApprentissage');
        } else {
            $listeEntreprises = $this->entreprise_m->getEntreprise();

            $entreprises = array();

            foreach ($listeEntreprises as $entreprise) {
                $entreprises[$entreprise->siren] = $entreprise->nom_entreprise;
            }

            $data = array('entreprises' => $entreprises, 'pagePrecedente' => $_SERVER['HTTP_REFERER'], 'title' => 'Ajouter une taxe d\'apprentissage');

            $this->template->load('template', 'taxeApprentissage/ajouterTaxeApprentissage', $data);
        }
    }

    public function listerTaxeApprentissage() {
        $data['message'] = 'Liste des taxes d\'apprentissages';
        $data['liste'] = $this->taxe_apprentissage_m->listerTaxeApprentissage();
        $data['title'] = 'Liste des taxes d\'apprentissages';
        $this->template->load('template', 'taxeApprentissage/listerTaxeApprentissage', $data);
    }

//
//    public function listerTaxeApprentissageEntreprise() {
//        $data['taxe'] = $this->taxeApprentissage_m($_GET['id']);
//    }
    
    public function recherche() {
        $taxe = $this->input->post('search');
        $data['message'] = 'Liste des taxes d\'apprentissages';
        $data['liste'] = $this->taxe_apprentissage_m->recherche($taxe);
        $data['title'] = 'Liste des taxes d\'apprentissages';
        $this->template->load('template', "taxeApprentissage/listerTaxeApprentissage", $data);
    }
}
