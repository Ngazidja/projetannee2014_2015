<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Interaction_m extends CI_Model {

    protected $table = 'interaction';

    public function inserer_interaction() {
        $rand = '';

        $this->idInteraction = $rand;
        $this->dateInteraction = $this->input->post('date');
        $this->objet = $this->input->post('objet');
        $this->entreprise = $this->input->post('entreprise');
        $this->source = $this->input->post('source');
        $this->etudiant = $this->input->post('etudiant');
        $this->description = $this->input->post('description');
        $this->auteur = $this->input->post('auteur');

        $this->db->insert($this->table, $this);
    }

    public function afficherHistorique() {
        $this->db->select("*", false)
                ->from('crm_interaction')
                ->join('crm_entreprise', 'crm_interaction.entreprise = crm_entreprise.siren')
                ->join('crm_etudiant', 'crm_interaction.etudiant = crm_etudiant.num_etudiant')
                ->order_by('dateInteraction', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function historiqueEtudiant($idEtudiant) {
        $etudiantHist = $this->db->select('*')
                ->from($this->table)
                ->join('crm_entreprise', 'crm_interaction.entreprise = crm_entreprise.siren')
                ->join('crm_etudiant', 'crm_interaction.etudiant = crm_etudiant.num_etudiant')
                ->where("etudiant =", $idEtudiant)
                ->order_by('dateInteraction', 'desc')
                ->get();
        return $etudiantHist->result();
    }

    public function historiqueEntreprise($idEntreprise) {
        $etudiantHist = $this->db->select('*')
                ->from($this->table)
                ->join('crm_entreprise', 'crm_interaction.entreprise = crm_entreprise.siren')
                ->join('crm_etudiant', 'crm_interaction.etudiant = crm_etudiant.num_etudiant')
                ->where("entreprise =", $idEntreprise)
                ->order_by('dateInteraction', 'desc')
                ->get()
                ->result();
        return $etudiantHist;
    }

    public function detailHistorique($id) {
        $historique = $this->db->get_where($this->table, array('idInteraction' => $id));
        $row = $historique->row();
        return $row;
    }

    public function recherche($nom) {
//        $query = $this->db->query("select *
//from crm_interaction, crm_etudiant, crm_entreprise
//where crm_interaction.entreprise = crm_entreprise.siren and crm_interaction.etudiant = crm_etudiant.num_etudiant and 
//nom_entreprise LIKE '" . $nom . "%' or nom_etudiant LIKE '" . $nom . "%' or prenom_etudiant LIKE '" . $nom . "%'
//order by  dateInteraction desc");
        $contacts = $this->db->select('*')
                ->from($this->table)
                ->join('crm_entreprise', 'crm_interaction.entreprise = crm_entreprise.siren')
                ->join('crm_etudiant', 'crm_interaction.etudiant = crm_etudiant.num_etudiant')
                ->where("nom_entreprise LIKE '" . $nom . "%' or nom_etudiant LIKE '" . $nom . "%' or prenom_etudiant LIKE '" . $nom . "%'")
                ->order_by('dateInteraction', 'desc')
                ->get();
        return $contacts->result();
//        return $query;
    }

}
