<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Taxe_apprentissage_m extends CI_Model {

    protected $taxe = 'taxeApprentissage';
    protected $entreprise = 'entreprise';

    /**
     *  Ajouter une taxe
     */
    public function ajouterTaxeApprentissage() {
        $this->idEntreprise = $this->input->post('idEntreprise');
        $this->annee = $this->input->post('annee');
        $this->montant = $this->input->post('montant');

        $this->db->insert($this->taxe, $this);
    }

    /*
     * Lister les taxes
     */

    public function listerTaxeApprentissage() {
        $query = $this->db->query('select  nom_entreprise, annee, montant
from crm_taxeApprentissage, crm_entreprise
where crm_taxeApprentissage.idEntreprise = crm_entreprise.siren order by annee desc');
        return $query->result();
    }

//    select  nom_entreprise, annee, montant
//from crm_taxeApprentissage, crm_entreprise
//where crm_taxeApprentissage.idEntreprise = crm_entreprise.siren

    public function listerTaxeApprentissageEntreprise() {
        $query = $this->db->query('select  nom_entreprise, annee, montant
from crm_taxeApprentissage, crm_entreprise
where crm_taxeApprentissage.idEntreprise = crm_entreprise.siren order by annee desc');
        return $query->result();
    }
    
    public function recherche($param) {
        $query = $this->db->query('select  nom_entreprise, annee, montant
from crm_taxeApprentissage, crm_entreprise
where crm_taxeApprentissage.idEntreprise = crm_entreprise.siren and nom_entreprise like "'.$param.'%"
order by annee desc');
        return $query->result();
    }

}

/* End of file TaxeApprentissage_m.php */