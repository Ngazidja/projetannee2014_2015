<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contact_m extends CI_Model {

    protected $table = 'contact';

    //Renommer la fonciton par ajouter contact
    public function inserer_Etudiant() {
        $this->entreprise = $this->input->post('entreprise');
        $this->poste = $this->input->post('poste');
        $this->nom_contact = $this->input->post('nom');
        $this->prenom_contact = $this->input->post('prenom');
        $this->telephone = $this->input->post('telephone');
        $this->email = $this->input->post('email');
        $this->db->insert($this->table, $this);
    }
    
    

    public function afficherTousLesContacts() {
        $contacts = $this->db->select('*')
                ->from($this->table)
                ->order_by('nom_contact', 'asc')
                ->get();
        return $contacts->result();
    }

    public function contactsEntrepise($entreprise) {
        $contactsEntreprise = $this->db->select('*')
                        ->from($this->table)
                        ->where("entreprise= ", $entreprise)
                        ->get()->result();
        return $contactsEntreprise;
    }

    public function detailContact($id) {
        $contact = $this->db->get_where($this->table, array('idContact' => $id));
        $row = $contact->row();
        return $row;
    }

    /* Il faut ajouter la possibilité de rechercher par le nom d'entreprise aussi */
    /* Recherche un contact */

    public function recherche($nom) {
        $contacts = $this->db->select('*')
                ->from($this->table)
                ->where("nom_contact LIKE '" . $nom . "%' or prenom_contact LIKE '" . $nom . "%' or poste LIKE '" . $nom . "%'")
                ->order_by('nom_contact', 'asc')
                ->get();
        return $contacts->result();
    }

}
