<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Entreprise_m extends CI_Model {

    protected $table = 'entreprise';

    /**
     *  Ajoute un entreprise
     */
    public function ajouter_entreprise($siren, $nom, $complementAdresse, $telephone, $email, $directeur, $fax, $numeroRue, $nomRue, $codePostal, $ville, $pays, $siege) {
        return $this->db->set(array('siren' => $siren, 'nom_entreprise' => $nom, 'complementAdresse' => $complementAdresse,
                            'telephone' => $telephone, 'email' => $email, 'directeur' => $directeur, 'fax' => $fax,
                            'numeroRue' => $numeroRue, 'nomRue' => $nomRue, 'codePostal' => $codePostal, 'ville' => $ville, 'pays' => $pays, 'siege' => $siege))
                        ->insert($this->table);
    }

    /**
     *  Modifie un entreprise
     */
    public function modifier_entreprise($siren, $nom, $adresse, $telephone, $email, $directeur, $fax, $numeroRue, $nomRue, $codePostal, $ville, $pays, $siege) {
        $data = array(
            'siren' => $siren,
            'nom' => $nom,
            'adresse' => $adresse,
            'telephone' => $telephone,
            'email' => $email,
            'directeur' => $directeur,
            'fax' => $fax,
            'numeroRue' => $numeroRue,
            'nomRue' => $nomRue,
            'codePostal' => $codePostal,
            'ville' => $ville,
            'pays' => $pays,
            'siege' => $siege
        );

        $this->db->where('siren', $siren);
        $this->db->update($this->table, $data);
    }

    /**
     *  Supprime une entreprise
     */
    public function supprimer_entreprise($idEntreprise) {
        $this->db->delete($this->table, array('siren' => $idEntreprise));
    }

    /**
     *  Retourne le nombre d'entreprise
     */
    public function count() {
        
    }

    /**
     *  Retourne une liste les entreprises
     */
    public function liste_entreprise() {
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            $rows = $query->result();
            return $rows;
        }
    }

    /**
     * Donne le détail d'une entreprise
     */
    public function details_entreprise($siren) {

        $query = $this->db->select('*')
                        ->from($this->table)
                        ->where('siren =', $siren)->get();
        return $query;
    }

    /*
     * Retourne la liste des entreprises
     */

    public function get_dropdown_entreprises() {
        $entreprises = $this->db->query('select siren from crm_entreprise');
        $dropdowns = $entreprises->result();
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->siren] = $dropdown->siren;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

    public function getAdresseEntreprise($idEntreprise) {
//        select siren, numeroRue, nomRue, codePostal, ville, pays from crm_entreprise where siren = '500'
        $query = $this->db->select('numeroRue, nomRue, codePostal, ville, pays')
                        ->from($this->table)->where('siren= ', $idEntreprise)->get()->row();
        return $query;
    }

    public function getNomEntreprise($idEntreprise) {
        $query = $this->db->select('nom_entreprise')
                        ->from($this->table)
                        ->where('siren= ', $idEntreprise)->get()->row();
        return $query;
    }

    //Code Mouhaimine
    public function getEntreprise() {
        return $this->db->select("siren, nom_entreprise")
                        ->order_by('nom_entreprise', 'asc')
                        ->get('crm_entreprise')
                        ->result();
    }

    public function recherche($nom) {
        $entreprise = $this->db->select('*')
                ->from($this->table)
                ->where("nom_entreprise LIKE '" . $nom . "%'")
                ->order_by('nom_entreprise', 'asc')
                ->get();
        return $entreprise->result();
    }
    
}

/* End of file entreprise_model.php */