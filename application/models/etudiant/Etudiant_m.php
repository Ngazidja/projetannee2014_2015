<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Etudiant_m extends CI_Model {

    protected $table = 'etudiant';
    protected $archive = 'archiveScolarite';

    /**
     *  Ajoute un etudiant
     */
    public function ajouter_etudiant($idEtu, $nom, $prenom, $dateNaissance, $tel, $email, $ine, $civilite, $complementAdresse, $numeroRue, $nomRue, $codePostal, $ville, $pays, $nationalite) {
        return $this->db->set(array('num_etudiant' => $idEtu, 'nom_etudiant' => $nom, 'prenom_etudiant' => $prenom,
                            'dateDeNaissance' => $dateNaissance, 'telephone' => $tel,
                            'email' => $email, 'ine' => $ine, 'civilite' => $civilite, 'complementAdresse' => $complementAdresse,
                            'numeroRue' => $numeroRue, 'nomRue' => $nomRue, 'codePostal' => $codePostal, 'ville' => $ville, 'pays' => $pays,
                            'nationalite' => $nationalite))
                        ->insert($this->table);
    }

    public function ajouterScolariteEtudiant($idEtudiant, $anneeDebut, $anneeFin, $niveau, $filiere) {
        return $this->db->set(array('idEtudiant' => $idEtudiant, 'anneeDebut' => $anneeDebut, 'anneeFin' => $anneeFin, 'niveau' => $niveau, 'filiere' => $filiere))->insert($this->archive);
    }

    /**
     *  Modifie un etudiant
     */
    public function modifier_etudiant($idEtu, $nom, $prenom, $dateNaissance, $tel, $email, $niveau, $ine, $civilite, $complementAdresse, $numeroRue, $nomRue, $codePostal, $ville, $pays, $filiere, $nationalite) {
        $data = array(
            'num_etudiant' => $idEtu,
            'nom' => $nom,
            'prenom' => $prenom,
            'dateDeNaissance' => $dateNaissance,
            'telephone' => $tel,
            'email' => $email,
            'niveau' => $niveau,
            'ine' => $ine,
            'civilite' => $civilite,
            'complementAdresse' => $complementAdresse,
            'numeroRue' => $numeroRue,
            'nomRue' => $nomRue,
            'codePostal' => $codePostal,
            'ville' => $ville,
            'pays' => $pays,
            'filiere' => $filiere,
            'nationalite' => $nationalite
        );

        $this->db->where('num_etudiant', $idEtu);
        $this->db->update($this->table, $data);
    }

    /**
     *  Supprime un etudiant
     */
    public function supprimer_etudiant($idEtudiant) {
        $this->db->delete($this->table, array('num_etudiant' => $idEtudiant));
    }

    /**
     *  Retourne le nombre d' etudiant
     */
    public function count() {
        
    }

    /**
     *  Retourne une liste d'étudiant
     */
    public function liste_etudiant() {
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            $rows = $query->result();
            return $rows;
        }
    }

    /*
     * Retourne le détail d'un étudiant
     */

    public function details_etudiant($id_etudiant) {

        $query = $this->db->select('*')
                        ->from($this->table)
                        ->where('num_etudiant = ', $id_etudiant)->get();
        return $query;
    }

    /*
     * Retourne le tableau de bord d'un étudiant, c'est à dire la liste de ses stages ou alternance
     */

    public function tableauDeBord($id_etudiant) {
        $query = $this->db->select('num_etudiant, annee, niveau, nom_entreprise')
                ->from('crm_convention, crm_etudiant, crm_entreprise, crm_archiveScolarite')
                ->where('crm_convention.idEtudiant = crm_etudiant.num_etudiant and '
                        . 'crm_convention.idEntreprise = crm_entreprise.siren '
                        . 'and crm_convention.idEtudiant = crm_archiveScolarite.idEtudiant '
                        . 'and crm_convention.idEtudiant = ', $id_etudiant)
                ->get();
        return $query;
    }

    public function getEtudiant() {
        return $this->db->select("num_etudiant,CONCAT(nom_etudiant,' ',prenom_etudiant) AS etudiant", false)
                        ->order_by('etudiant', 'asc')
                        ->get('crm_etudiant')
                        ->result();
    }

    public function recherche($nom) {
        $contacts = $this->db->select('*')
                ->from($this->table)
                ->where("nom_etudiant LIKE '" . $nom . "%'")
                ->order_by('nom_etudiant', 'asc')
                ->get();
        return $contacts->result();
    }

    public function scolarite($idEtudiant) {
        $query = $this->db->select('*')
                ->from($this->archive)
                ->where("idEtudiant =", $idEtudiant)
                ->order_by('anneeDebut', 'desc')
                ->get()
                ->result();
        return $query;
    }

}

/* End of file etudiant_model.php */