<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Convention_m extends CI_Model {

    protected $table = 'convention';

    /*
     * Retourne le détail d'un contrat
     */

    public function detailsContrat($id_etudiant, $annee) {
        $query = $this->db->query("select * FROM crm_convention, crm_etudiant, crm_entreprise where(crm_convention.idEtudiant = crm_etudiant.num_etudiant and crm_convention.idEntreprise = crm_entreprise.siren and idEtudiant = '" . $id_etudiant . "' and annee = '" . $annee . "');");
//       
        return $query;
    }

    /*
     * Ajoute un contrat
     */

    public function ajouter_contrat($idEtudiant, $idEntreprise, $poste, $annee, $duree, $remuneration, $tuteurEntreprise, $tuteurPedagogique, $type, $alternance) {
        return $this->db->set(array('idConvention' => '',
                            'idEtudiant' => $idEtudiant,
                            'idEntreprise' => $idEntreprise,
                            'poste' => $poste,
                            'annee' => $annee,
                            'duree' => $duree,
                            'remuneration' => $remuneration,
                            'tuteurEntreprise' => $tuteurEntreprise,
                            'tuteurPedagogique' => $tuteurPedagogique,
                            'type' => $type,
                            'alternance' => $alternance
                        ))
                        ->insert($this->table);
    }

    public function recherche($nom) {
        $contacts = $this->db->select('*')
                ->from($this->table)
                ->where("nom_contact LIKE '" . $nom . "%'")
                ->order_by('nom_contact', 'asc')
                ->get();
        return $contacts->result();
    }

    public function contratsEntreprise($idEntreprise) {
        $contracts = $this->db->select('*')
                        ->from($this->table)
                        ->where("idEntreprise = '" . $idEntreprise . "'")
                        ->get()->result();
        return $contracts;
    }

}
